from django.test import TestCase
from django.contrib.auth import get_user_model

import datetime
from django.utils import timezone

User = get_user_model()

from .models import *
from home.models import Chipcard, Product

class LedgerTestCase(TestCase):

    def test_account_name(self):
        t = Transaction.objects.create(payee='Testkauf')
        c, created = Chipcard.objects.get_or_create(
            serial='12345678',
        )
        self.assertTrue(created)
        p = Posting.objects.create(transaction=t, chipcard=c, amount='13.37')
        self.assertEqual('assets:users:User@12345678', p.account_name_get())

        u = User.objects.create(username='testuser')
        c.user = u
        c.save()
        c, created = Chipcard.objects.get_or_create(serial='12345678')
        self.assertFalse(created)

        p = Posting.objects.create(transaction=t, chipcard=c, amount='47.11')
        self.assertEqual('assets:users:' + u.username, p.account_name_get())

    def test_get_histogram_per_day_of_week(self):
        #  c, created = Chipcard.objects.get_or_create(
            #  serial='12345678',
        #  )
        Product.objects.create(name='Mate', price='0.7')

        t1 = Transaction.objects.create(payee='Testkauf Gestern')
        t1.date = timezone.now() - datetime.timedelta(days=1)
        t1.save()
        Posting.objects.create(transaction=t1,
                account_name='sold:drinks:Mate', amount='1', unit='F')
        Posting.objects.create(transaction=t1,
                account_name='assets:drinks:Mate', amount='1', unit='-F')

        t2 = Transaction.objects.create(payee='Testkauf Heute')
        Posting.objects.create(transaction=t2,
                account_name='sold:drinks:Mate', amount='1', unit='F')
        Posting.objects.create(transaction=t2,
                account_name='assets:drinks:Mate', amount='1', unit='-F')

        h = Ledger.get_histogram_per_day_of_week('sold:drinks:Mate', unit='F')
        self.assertEqual(h.count(), 2)
        self.assertEqual(h[0].get('count'), 1)
        self.assertEqual(h[1].get('count'), 1)
