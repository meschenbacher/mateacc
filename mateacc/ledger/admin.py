from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import *


class PostingInline(admin.TabularInline):
    model = Posting


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'date',
        'date2',
        'status',
        'payee',
        'comment',
        #  'get_user',
    )
    list_filter = [
        'posting__chipcard__user',
        'posting__chipcard',
    ]
    inlines = [
        PostingInline,
    ]
    readonly_fields = (
        'date',
    )

    #  def get_user(self, obj):
        #  return Posting.objects.filter(transaction=obj, )


@admin.register(Posting)
class PostingAdmin(admin.ModelAdmin):
    def transaction_date(self, obj):
        return obj.transaction.date
    list_display = (
        'pk',
        'amount',
        'unit',
        'transaction',
        'transaction_date',
        'account_name',
        'chipcard',
        'date',
        'status',
        'comment',
    )
    list_filter = [
        'chipcard',
        'account_name',
        'unit',
        'status',
    ]
    list_display_links = [
        'pk',
        #  'transaction',
        #  'chipcard',
    ]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass
