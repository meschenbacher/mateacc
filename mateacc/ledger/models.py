from django.db import models
from django.utils.translation import ugettext as _
from django.db.models import Sum, Count, Value as V
from django.db.models.functions import Coalesce, Extract, ExtractHour

from core.fields import EnumField

STATUS_CHOICES = [
    ('', _('(none)')),
    ('!', _('pending')),
    ('*', _('cleared')),
]

class Transaction(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    date2 = models.DateTimeField(blank=True, null=True)
    status = EnumField(choices=STATUS_CHOICES, default='', blank=True)
    payee = models.CharField(max_length=255)
    comment = models.CharField(max_length=255, default='', blank=True)

    def __str__(self):
        return 'Transaction [%(payee)s] [%(pk)s]' % {
            'payee': self.payee,
            'pk': self.pk,
        }


class Posting(models.Model):
    def __init__(self, *args, **kwargs):
        """
        the init method overrides the account_name if a chipcard is assigned
        """
        super().__init__(*args, **kwargs)

    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    status = EnumField(choices=STATUS_CHOICES, default='', blank=True)
    account_name = models.CharField(max_length=255, blank=True, null=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    unit = models.CharField(max_length=255, blank=True, null=True)
    comment = models.CharField(max_length=255, default='', blank=True)
    date = models.DateTimeField(blank=True, null=True)
    chipcard = models.ForeignKey(
        'home.Chipcard',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def account_name_get(self):
        if self.chipcard:
            acc_string = 'assets:users:%(username)s' % {
                'username': self.chipcard.user.username,
            }
            return acc_string
        return self.account_name


class Tag(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255, blank=True)


class Ledger():
    @staticmethod
    def balance(account_name, unit=None):
        aggregate = Posting.objects.filter(account_name=account_name,
                unit=unit).values('account_name').annotate(sum = Coalesce(Sum('amount'), V(0)))
        if aggregate.count() == 0:
            return 0
        return aggregate[0].get('sum')

    @staticmethod
    def balance_chipcard(chipcard, unit=None):
        aggregate = Posting.objects.filter(chipcard=chipcard,
                unit=unit).values('account_name').annotate(sum = Coalesce(Sum('amount'), V(0)))
        if aggregate.count() == 0:
            return 0
        return aggregate[0].get('sum')
    @staticmethod
    def get_histogram_per_day_of_week(account_name, unit=None):
        aggregate = Posting.objects.filter(
            account_name=account_name, unit=unit
        ).annotate(
            week_day=Extract('transaction__date', 'week_day'),
        ).values(
            'week_day'
        ).annotate(
            count = Count('id'),
        ).order_by('week_day')
        #  if aggregate.count() == 0:
            #  return {}
        return aggregate

    @staticmethod
    def get_histogram_per_hour(account_name, unit=None):
        aggregate = Posting.objects.filter(
            account_name=account_name, unit=unit
        ).annotate(
            hour=ExtractHour('transaction__date'),
        ).values(
            'hour'
        ).annotate(
            count = Count('id'),
        ).order_by('hour')
        #  if aggregate.count() == 0:
            #  return {}
        return aggregate
