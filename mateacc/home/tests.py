from django.test import TestCase
from django.test.client import Client

from .models import Product
from ledger.models import Transaction

class HomeModelsTestCase(TestCase):
    def setUp(self):
        self.mate = Product.objects.create(name='Mate', price='0.7')

    def test_product_buy(self):
        count = Transaction.objects.count()
        Product.buy(name='Mate', serial='12345678')
        self.assertEqual(count + 1, Transaction.objects.count())
        Product.buy(name='Mate', serial='12345678')
        self.assertEqual(count + 2, Transaction.objects.count())

class HomeViewsTestCase(TestCase):
    def setUp(self):
        self.mate = Product.objects.create(name='Mate', price='0.7',
                account_name_assets='assets:drinks:Mate',
                account_name_goods='goods:drinks:Mate',
                account_name_expenses='expenses:drinks:Mate',
        )
        self.client = Client()

    def test_replenish(self):
        count = Transaction.objects.count()
        Product.buy(name='Mate', serial='12345678')
        self.assertEqual(count + 1, Transaction.objects.count())
        Product.buy(name='Mate', serial='12345678')
        self.assertEqual(count + 2, Transaction.objects.count())
