from django import forms

from .models import Chipcard, Product


class DepositForm(forms.Form):
    chipcard = forms.ModelChoiceField(queryset=Chipcard.objects.all())
    amount = forms.DecimalField()


class ReplenishmentForm(forms.Form):
    num_bottles = forms.IntegerField()
    product = forms.ModelChoiceField(queryset=Product.objects.all())
    total_price = forms.DecimalField()
