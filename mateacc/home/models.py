from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
#from django.utils.translation import ugettext_lazy as _

from core.models import CreatedUpdatedModel, CreatedModel
from ledger.models import Transaction, Posting

User = get_user_model()

# TODO remove powerUsers and save data with the user model
powerUsers = [
    # peter weinberger
    '045C7232795B80',
    # maximilian eschenbacher
    '046A60FA1E5680',
]


class ChipcardManager(models.Manager):

    def get_or_create(self, defaults=None, **kwargs):
        """
        this overrides the get_or_create or models.Manager
        to create a user with the chipcard
        """
        try:
            chipcard = self.get(**kwargs)
            return chipcard, False
        except Chipcard.DoesNotExist:
            if not defaults or 'user' not in defaults:
                user = User.objects.create(username='User@'+kwargs.get('serial'))
            else:
                user = defaults['user']
            chipcard = self.create(user=user, **kwargs)
            return chipcard, True


chipcard_serial_regex_validator = RegexValidator(
    regex='^[a-fA-F0-9]{8,14}$',
    message=_('Must be a valid MIFARE or DESFIRE serial number (8 or 14 characters)'),
    code='invalid_format',
)


class Chipcard(CreatedUpdatedModel):
    serial = models.CharField(
        _('serial number'),
        max_length=14,
        validators=[
            chipcard_serial_regex_validator,
        ]
    )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    objects = ChipcardManager()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
    def __str__(self):
        return 'Chipcard [0x%s] [%s]' % (self.serial, self.user)


class Product(CreatedUpdatedModel):
    name = models.CharField(
        _('name'),
        max_length=255,
        unique=True,
    )
    account_name_assets = models.CharField(
        _('account name assets'),
        max_length=255,
    )
    account_name_goods = models.CharField(
        _('account name purchase'),
        max_length=255,
    )
    account_name_expenses = models.CharField(
        _('account name expenses'),
        max_length=255,
    )
    price = models.DecimalField(
        _('price'),
        max_digits=8,
        decimal_places=2,
    )

    def __str__(self):
        return '%s (%.2f)' % (self.name, self.price)

    @staticmethod
    def buy(name, serial):
        try:
            chipcard_serial_regex_validator(serial)
        except ValidationError as e:
            logging.exception(e)
            return None

        # TODO fix logging of db errors with LOGGING
        chipcard, created = Chipcard.objects.get_or_create(serial=serial)
        product = Product.objects.get(name=name)
        t = Transaction.objects.create(payee='Kauf')
        Posting.objects.create(
            transaction=t,
            chipcard=chipcard,
            amount='-' + str(product.price),
        )
        Posting.objects.create(
            transaction=t,
            account_name='assets:digital',
            amount=product.price,
        )
        Posting.objects.create(
            transaction=t,
            account_name='assets:drinks:%(product_name)s' % {'product_name': product.name},
            amount='-1',
            unit='F'
        )
        Posting.objects.create(
            transaction=t,
            account_name='sold:drinks:%(product_name)s' % {'product_name': product.name},
            amount='1',
            unit='F'
        )
        if serial in powerUsers:
            return ['general', 'special']

        return ['general']


#  class Purchase(CreatedModel):
    #  chipcard = models.ForeignKey(
        #  Chipcard,
        #  on_delete=models.CASCADE,
    #  )
    #  product = models.ForeignKey(
        #  Product,
        #  on_delete=models.CASCADE,
    #  )
    #  price = models.DecimalField(
        #  _('price'),
        #  max_digits=8,
        #  decimal_places=2,
    #  )
#  
#  
#  class Stockup(CreatedModel):
    #  product = models.ForeignKey(
        #  Product,
        #  on_delete=models.CASCADE,
    #  )
    #  count = models.PositiveSmallIntegerField()
