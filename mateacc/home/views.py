from django.shortcuts import render, reverse, redirect
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django_tables2 import RequestConfig
from django.utils.translation import ugettext as _
import django_tables2 as tables
from django.db.models import Q
from django.contrib.auth import get_user_model


from ledger.models import Transaction, Posting, Ledger
from .models import Chipcard
from .forms import DepositForm, ReplenishmentForm


User = get_user_model()


class TransactionTable(tables.Table):
    # access date of the associated transaction because the date of the posting
    # is most likely empty
    transaction_date = tables.Column(verbose_name=_('transaction date'), accessor='transaction.date')
    user = tables.Column(verbose_name=_('user'), accessor='chipcard.user')
    class Meta:
        model = Posting
        template_name = 'django_tables2/bootstrap.html'
        exclude = ('transaction', 'status', 'account_name', 'comment', 'date')
        order_by = ('-date')


class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        table = TransactionTable(Posting.objects.filter(chipcard__user=request.user).prefetch_related('chipcard'))
        RequestConfig(request).configure(table)
        return render(request, 'home/index.dhtml', dict(
            mate_count=Ledger.balance('assets:drinks:Mate', unit='F'),
            balance_kasse=Ledger.balance('assets:kasse'),
            balance_digital=Ledger.balance('assets:digital'),
            balance_pfand=Ledger.balance('assets:pfand'),
            balance_users=[
                dict(
                    user=user,
                    #  sum=Ledger.balance('assets:users:%s' % (user.username,)),
                    sum=sum(
                        [
                            Ledger.balance_chipcard(card) for card in Chipcard.objects.filter(user=user)
                        ]
                    ),
                ) for user in User.objects.all()
            ],
            table=table,
            histogram=Ledger.get_histogram_per_day_of_week('sold:drinks:Mate', unit='F'),
            histogram_hour=Ledger.get_histogram_per_hour('sold:drinks:Mate', unit='F'),
        ))


class InventoryView(LoginRequiredMixin, View):
    def get(self, request):
        purchases = Transaction.objects.filter(
                posting__account_name='assets:drinks:Mate')
        return render(request, 'home/inventory.dhtml', dict(
            purchases=purchases,
        ))


class DepositView(PermissionRequiredMixin, View):
    permission_required = ('transaction.can_add', 'posting.can_add')
    def get(self, request):
        return render(request, 'home/deposit.dhtml', dict(
            form=DepositForm(),
        ))
    def post(self, request):
        form = DepositForm(request.POST)
        if not form.is_valid():
            print('error')
            return redirect(reverse('deposit'))
        t = Transaction.objects.create(payee='Einzahlung')
        chipcard = form.cleaned_data['chipcard']
        Posting.objects.create(
            transaction=t,
            chipcard=chipcard,
            amount=form.cleaned_data['amount'],
        )
        Posting.objects.create(
            transaction=t,
            account_name='income:deposit',
            amount=0 - form.cleaned_data['amount'],
        )

        return redirect(reverse('index'))


class ReplenishmentView(PermissionRequiredMixin, View):
    permission_required = ('transaction.can_add', 'posting.can_add')
    def get(self, request):
        return render(request, 'home/replenishment.dhtml', dict(
            form=ReplenishmentForm(),
        ))
    def post(self, request):
        form = ReplenishmentForm(request.POST)
        if not form.is_valid():
            print('error')
            return redirect(reverse('replenishment'))
        t = Transaction.objects.create(payee='Bestellung')
        num_bottles = form.cleaned_data['num_bottles']
        amount = form.cleaned_data['total_price']
        product = form.cleaned_data['product']
        Posting.objects.create(
            transaction=t,
            account_name=product.account_name_assets,
            amount=num_bottles,
            unit='F',
        )
        Posting.objects.create(
            transaction=t,
            account_name=product.account_name_goods,
            amount=0-num_bottles,
            unit='F',
        )
        Posting.objects.create(
            transaction=t,
            account_name='assets:kasse',
            amount=0-amount,
        )
        Posting.objects.create(
            transaction=t,
            account_name=product.account_name_expenses,
            amount=amount,
        )

        return redirect(reverse('index'))
