from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import *


class UserIsNullFilter(admin.SimpleListFilter):
    title = _('chipcards without user')
    parameter_name = 'decade'
    def lookups(self, request, model_admin):
        return (
            ('isnull', _('is null')),
        )
    def queryset(self, request, queryset):
        if self.value() == 'isnull':
            return queryset.filter(user__isnull=True)


class ChipcardWithoutPostingFilter(admin.SimpleListFilter):
    title = _('chipcards without posting')
    parameter_name = 'without_posting'
    def lookups(self, request, model_admin):
        return (
            ('without', _('without posting')),
        )
    def queryset(self, request, queryset):
        if self.value() == 'without':
            return queryset.filter(posting__isnull=True)


@admin.register(Chipcard)
class ChipcardAdmin(admin.ModelAdmin):
    list_display = (
        'serial',
        'user',
        'created',
    )
    list_filter = (
        UserIsNullFilter,
        ChipcardWithoutPostingFilter,
    )

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'created',
        'updated',
    )

#  @admin.register(Purchase)
#  class PurchaseAdmin(admin.ModelAdmin):
    #  list_display = (
        #  'pk',
        #  'chipcard',
        #  'product',
        #  'price',
        #  'created',
    #  )
    #  readonly_fields = (
        #  'chipcard',
        #  'product',
        #  'price',
        #  'created',
    #  )
#  
#  @admin.register(Stockup)
#  class StockupAdmin(admin.ModelAdmin):
    #  list_display = (
        #  'pk',
        #  'product',
        #  'count',
    #  )
