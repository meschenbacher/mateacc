#!/usr/bin/env python3
"""
Sample script that monitors smartcard insertion/removal.

__author__ = "http://www.gemalto.com"

Copyright 2001-2012 gemalto
Author: Jean-Daniel Aussel, mailto:jean-daniel.aussel@gemalto.com

This file is part of pyscard.

pyscard is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

pyscard is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyscard; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from __future__ import print_function
from time import sleep
import os
import sys
import random
from subprocess import call, DEVNULL, PIPE, STDOUT
import datetime
import logging

from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.util import toHexString
from smartcard.System import readers

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings_local")
django.setup()

from django.conf import settings

from home.models import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

r = None

def absoluteFilePaths(directories):
    for directory in directories:
        for dirpath,_,filenames in os.walk(directory):
            for f in filenames:
                yield os.path.abspath(os.path.join(dirpath, f))

def build_paths(categories):
    pass

def handle_card(product_name, uid):

    """
    handle two special cards for changing volume
    """
    if uid == 'E183A6A9':
        p = call(['amixer', 'set', 'PCM', '8dB+'], stdout=PIPE, stderr=PIPE)
        sound_category_names = ['general', 'special']
    elif uid == 'F17FA6A9':
        p = call(['amixer', 'set', 'PCM', '8dB-'], stdout=PIPE, stderr=PIPE)
        sound_category_names = ['general', 'special']
    else:
        """
        buy product
        """
        try:
            sound_category_names = Product.buy(product_name, uid)
        except Exception as e:
            logging.exception(e)
            raise e

    sound_base_dirs = []
    possible_sound_files = []

    for sound_category_name in sound_category_names:
        sound_base_dir = '%(base)s/sounds/%(category)s' % {
            'base': settings.STATIC_ROOT,
            'category': sound_category_name,
        }
        sound_base_dirs.append(sound_base_dir)

    possible_sound_files = list(absoluteFilePaths(sound_base_dirs))
    sound_file = random.choice(possible_sound_files)
    # IMPORTANT: user must be in group audio
    p = call(['play', sound_file], stdout=PIPE, stderr=PIPE)


# a simple card observer that prints inserted/removed cards
class PrintObserver(CardObserver):
    """A simple card observer that is notified
    when cards are inserted/removed from the system and
    prints the list of cards
    """

    def update(self, observable, actions):
        try:
            (addedcards, removedcards) = actions
            for card in addedcards:
                #  print("+Inserted: ", toHexString(card.atr), card.__dict__)
                connection = r.createConnection()
                connection.connect()
                # transmit no buzzer (see manual)
                select = [0xA0, 0xA4, 0x00, 0x00, 0x02]
                set_led = [0xFF, 0x00, 0x40, 0x0F, 0x04, 0x00, 0x00, 0x00, 0x00]
                # https://stackoverflow.com/questions/18219390/disable-the-default-red-led-on-the-acr122u
                disable_buzzer = [0xFF, 0x00, 0x52, 0x00, 0x00]
                data, sw1, sw2 = connection.transmit(disable_buzzer)
                if (sw1, sw2) != (144, 0x0):
                    logging.warning("setting led and buzzer failed (data, sw1, sw2) = ([%s], [%s], [%s])", data, sw1, sw2)
                # transmit getuid command
                data, sw1, sw2 = connection.transmit([0xFF, 0xCA, 0x00, 0x00, 0x00])
                connection.disconnect()
                uid = toHexString(data).replace(' ', '')
                product_name = 'Mate'
                logging.info("%s start buy uid %s product %s", datetime.datetime.now(), uid, product_name)
                handle_card(product_name, uid)
                logging.info("%s end buy uid %s product %s", datetime.datetime.now(), uid, product_name)
                #  data, sw1, sw2 = connection.transmit([0x00, 0x00, 0x00, 0x00])
            #  for card in removedcards:
                #  print("-Removed: ", toHexString(card.atr), card.__dict__)
        except Exception as e:
            logging.exception(e)

def loop():
    num_reader_initial = len(readers())
    cardmonitor = CardMonitor()
    cardobserver = PrintObserver()
    cardmonitor.addObserver(cardobserver)

    while True:
        sleep(10)
    #  sleep(10)

    # don't forget to remove observer, or the
    # monitor will poll forever...
    cardmonitor.deleteObserver(cardobserver)

if __name__ == '__main__':
    if '--simulate' in sys.argv:
        buy('Mate', '12345678')
        sys.exit(0)
    r = readers()[0]
    loop()
