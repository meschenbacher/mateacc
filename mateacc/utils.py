from email import message_from_string

from django.template import loader, Context
from django.core.mail import EmailMessage

def email(template, context=dict()):
    """
    Sends mail using template template(must be in TEMPLATE_DIRS) and
    context
    """

    t = loader.get_template(template)
    thistext = t.render(context)

    header, body = thistext.split("\n\n", 1)

    msg = message_from_string(header)

    mail = EmailMessage(subject=msg.get("Subject"),
                        body=body,
                        from_email=msg.get("From"),
                        to=msg.get_all("To"),
                        bcc=msg.get_all("Bcc"),
                        cc=msg.get_all("Cc"),
                        reply_to=msg.get_all("Reply-To"))
    mail.send(fail_silently=False)
