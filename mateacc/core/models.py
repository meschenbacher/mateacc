from django.db import models
from django.utils.translation import ugettext as _


class CreatedModel(models.Model):
    created = models.DateTimeField(
        _('created'),
        auto_now_add=True,
        help_text=_('this field will be set to the datetime of creation automatically'),
    )
    class Meta:
        abstract = True


class CreatedUpdatedModel(CreatedModel):
    updated = models.DateTimeField(
        _('last modified'),
        auto_now=True,
        help_text=_('this field will be set to the datetime of modification automatically'),
    )
    class Meta:
        abstract = True
