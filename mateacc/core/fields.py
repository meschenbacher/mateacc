from django.db import models
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


"""
Enum class from https://stackoverflow.com/questions/9632521/django-handling-enum-models
"""
class EnumField(models.CharField):

    def __init__(self, *args, **kwargs):
        if 'choices' not in kwargs:
            raise AttributeError(_('EnumField requires `choices` attribute.'))
        kwargs['max_length'] = max([len(s) for (s, _) in kwargs['choices']])
        super(EnumField, self).__init__(*args, **kwargs)

    def db_type(self, connection):

        """
        fallback for sqlite enum to string
        """
        from django.db.backends.sqlite3.base import DatabaseWrapper as SQLiteDatabaseWrapper
        from django.db.backends.postgresql.base import DatabaseWrapper as Psycopg2DatabaseWrapper
        if isinstance(connection, SQLiteDatabaseWrapper) or \
           isinstance(connection, Psycopg2DatabaseWrapper):
            return "VARCHAR(%d)" % (max(max(len(a), len(b)) for (a, b) in self.choices))

        """
        regular database should support enum
        """
        return "enum(%s)" % ','.join("'%s'" % k for (k, _) in self.choices)
