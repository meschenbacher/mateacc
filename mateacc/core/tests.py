from django.test import TestCase

from .fields import EnumField

class EnumFieldModelTest(TestCase):
    def test_constructor(self):
        with self.assertRaises(AttributeError):
            f = EnumField()
        f = EnumField(choices=[('val1', 'val1'), ('val2', 'val2')])
