from .settings_base import *

SECRET_KEY = '+3s+$i!51$#!o%j_cwwjw!7p($10q8-oo5xcq7u3mf@8@i@$kq'
DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = [
    '127.0.0.1',
    '::1',
]

DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': os.path.join(STATIC_URL, 'js/jquery-1.12.4.min.js'),
}
