# Mateacc Mate Accounting with Django

Beware, this is very hacky and ugly but working.

# Architecture

There is a nfcreader-equipped device somewhere on which systemd `contrib/mateacc.service` is
running. The reader reads a card's serial number and books makes a transaction.
This is called the django `production`. The user which executes `reader.py` must be in the
group `audio`.

The webinterface (mostly django-admin with ugly statistics can be deployed on
the same machine or somewhere else. The two services only have to use the same
database.

# Requirements

`python3-dev python3-pyscard pcscd pcsc-tools libpcsclite-dev`
`sox libsox-fmt-mp3`

`swig libpcsclite-dev` for building pyscard in requirements\_base

## Additional for production and webinterface

`python3-psycopg2` (which will bring shared libraries to be used by pip's
psycopg2-binary)

# How to install for local development (checked for Debian Stretch)

```
# install required packages on your system
$ apt install ...
# set up virtualenv
virtualenv --python=python3 virtualenv
source virtualenv/bin/activate
pip install -r requirements_local.txt
make migrate
make loaddata
make compilemessages
```


Install udev rules into `/etc/udev/rules.d` and add the user to `plugdev` group.
[foo](https://github.com/nfc-tools/libnfc/blob/master/contrib/udev/93-pn53x.rules)

Blacklist `pn533 pn533_usb` kernel modules by entering the into
`/etc/modul<TAB>/blacklist.conf`

Install `pcscd` and `pcsc-tools`, if `pcsc_scan` succeeds, everything is set up
properly.

# Links

Important and good https://github.com/nfc-tools/libnfc/

https://github.com/StevenTso/ACS-ACR122U-NFC-Reader
https://wiki.debian.org/KernelModuleBlacklisting
http://nfc-tools.org/index.php?title=Libnfc
https://stackoverflow.com/questions/36511901/python-acr122u-poll#36516204


python beep stuff https://github.com/rocky112358/ACS-ACR122U-Tool

# pcscd pcsc-tools

without `pn533_usb pn533 nfc`
use `pcsc_scan`
or python smartcard stuff

now use https://pyscard.sourceforge.io/user-guide.html#listing-smartcard-readers
udev rules

https://oneguyoneblog.com/2016/11/02/acr122u-nfc-usb-reader-linux-mint/
