# how to setup reader

```
grant select,insert ON home_chipcard TO mateacc_production1 ;
grant select,insert ON home_product TO mateacc_production1;
grant select,insert ON ledger_transaction TO mateacc_production1;
grant select,insert on ledger_posting to mateacc_production1 ;
grant select,insert on auth_user to mateacc_production1 ;
grant USAGE on home_chipcard_id_seq to mateacc_production1 ;
grant USAGE on ledger_transaction_id_seq to mateacc_production1 ;
grant USAGE on ledger_posting_id_seq to mateacc_production1 ;
grant USAGE on auth_user_id_seq to mateacc_production1 ;
```
